include $(TOPDIR)/rules.mk

PKG_NAME:=quickjs
PKG_RELEASE:=1

PKG_SOURCE_PROTO:=git
PKG_SOURCE_URL:=https://github.com/bellard/quickjs.git
PKG_SOURCE_DATE:=2020-11-08
PKG_SOURCE_VERSION:=204682fb87ab9312f0cf81f959ecd181180457bc
PKG_MIRROR_HASH:=skip
PKG_LICENSE:=MIT

PKG_INSTALL:=1
PKG_BUILD_PARALLEL:=1

include $(INCLUDE_DIR)/package.mk

define Package/quickjs
  SECTION:=lang
  CATEGORY:=Languages
  TITLE:=QuickJS Javascript engine
  URL:=https://bellard.org/quickjs/
  MAINTAINER:=Erik Karlsson <erik.karlsson@genexis.eu>
  DEPENDS:=+libatomic
endef

define Package/quickjs/description
  QuickJS is a small and embeddable Javascript engine. It supports
  the ES2020 specification including modules, asynchronous
  generators, proxies and BigInt.
endef

MAKE_VARS += \
	LIBS="-latomic"

MAKE_FLAGS = \
	prefix=/usr \
	CONFIG_SMALL=y \
	CROSS_PREFIX="$(TARGET_CROSS)"

define Build/InstallDev
	$(INSTALL_DIR) $(1)/usr/lib/quickjs
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/quickjs/libquickjs.a $(1)/usr/lib/quickjs/
	$(CP) $(PKG_INSTALL_DIR)/usr/lib/quickjs/libquickjs.lto.a $(1)/usr/lib/quickjs/
	$(INSTALL_DIR) $(1)/usr/include/quickjs
	$(CP) $(PKG_INSTALL_DIR)/usr/include/quickjs/quickjs.h $(1)/usr/include/quickjs/
	$(CP) $(PKG_INSTALL_DIR)/usr/include/quickjs/quickjs-libc.h $(1)/usr/include/quickjs/
endef

define Package/quickjs/install
	$(INSTALL_DIR) $(1)/usr/bin
	$(INSTALL_BIN) $(PKG_INSTALL_DIR)/usr/bin/qjs $(1)/usr/bin/
endef

$(eval $(call BuildPackage,quickjs))
