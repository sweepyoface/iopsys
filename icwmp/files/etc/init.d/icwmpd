#!/bin/sh /etc/rc.common
# Copyright (C) 2015-2019 iopsys Software Solutions AB

. /lib/functions.sh
include /lib/network
. /usr/share/libubox/jshn.sh

START=99
STOP=2

USE_PROCD=1
PROG="/usr/sbin/icwmpd"

log() {
	echo "${@}"|logger -t cwmp.init -p info
}

enable_dhcp_option43() {
	local wan="${1}"

	### Ask for DHCP Option 43 only if CWMP is enabled ###
	local reqopts="$(uci -q get network.$wan.reqopts)"
	local proto="$(uci -q get network.$wan.proto)"
	local newreqopts=""
	local option43_present=0

	for ropt in $reqopts; do
		case $ropt in
			43) option43_present=1 ;;
			*) ;;
		esac
	done

	if [ ${option43_present} -eq 1 ]; then
		return;
	fi

	newreqopts="$reqopts 43"
	if [ $proto == "dhcp" ]; then
		uci -q set network.$wan.reqopts="$newreqopts"
		uci commit network
		ubus call network reload
	fi
}

wait_for_resolvfile() {
	local time=$1
	local tm=1

	local resolvfile="$(uci -q get dhcp.@dnsmasq[0].resolvfile)"
	[ -n "$resolvfile" ] || return

	while [ ! -f $resolvfile ]; do
		sleep 1
		[ $tm -ge $time ] && break
		tm=$((tm+1))
	done
}

get_default_wan_interface() {
	local default_wan_interface

	config_load cwmp
	config_get default_wan_interface cpe default_wan_interface "wan"

	echo ${default_wan_interface}
}

set_wan_interface() {
	local wan_interface="${1}"
	local l3_device=""

	if [ -z "${wan_interface}" ]; then
		return 0;
	fi

	json_load "$(ifstatus ${wan_interface})"
	json_get_var l3_device l3_device
	if [ -n "$l3_device" ]; then
		uci -q set cwmp.cpe.interface="${l3_device}"
		uci -q commit cwmp
	fi
}

copy_cwmp_etc_files_to_varstate() {
	if [ ! -f /var/state/cwmp ]
	then
		if [ -f /etc/icwmpd/cwmp ]
		then
			uci -q -c /etc/icwmpd delete cwmp.acs
			uci -q -c /etc/icwmpd commit cwmp
			cp /etc/icwmpd/cwmp /var/state/cwmp
		else
			touch /var/state/cwmp
		fi
	fi

	mkdir -p /var/state/icwmpd
	if [ -f /etc/icwmpd/.icwmpd_backup_session.xml ]
	then
		mv /etc/icwmpd/.icwmpd_backup_session.xml /var/state/icwmpd 2>/dev/null
	fi
	if [ -f /etc/icwmpd/.dm_enabled_notify.xml ]
	then
		mv /etc/icwmpd/.dm_enabled_notify /var/state/icwmpd 2>/dev/null
	fi
}

copy_cwmp_varstate_files_to_etc() {
	if [ -f /var/state/cwmp ]
	then
		cp /var/state/cwmp /etc/icwmpd 2>/dev/null
	fi
	if [ -f /var/state/icwmpd/.icwmpd_backup_session.xml ]
	then
		cp /var/state/icwmpd/.icwmpd_backup_session.xml /etc/icwmpd 2>/dev/null
	fi
	if [ -f /var/state/icwmpd/.dm_enabled_notify.xml ]
	then
		cp /var/state/icwmpd/.dm_enabled_notify /etc/icwmpd 2>/dev/null
	fi
}

validate_acs_section()
{
	uci_validate_section cwmp acs "acs" \
		'passwd:string' \
		'periodic_inform_enable:bool' \
		'periodic_inform_interval:uinteger' \
		'periodic_inform_time:string' \
		'url:string' \
		'dhcp_url:string' \
		'dhcp_discovery:or("enable", "disable")' \
		'compression:or("GZIP","Deflate","Disabled")' \
		'retry_min_wait_interval:range(1, 65535)' \
		'retry_interval_multiplier:range(1000, 65535)' \
		'https_ssl_capath:file' \
		'ipv6_enable:bool'

}

validate_cpe_section()
{
	uci_validate_section cwmp cpe "cpe" \
		'interface:string' \
		'default_wan_interface:string' \
		'log_to_console:or("enable","disable")' \
		'log_to_file:or("enable","disable")' \
		'log_severity:or("EMERG", "ALERT", "CRITIC" ,"ERROR", "WARNING", "NOTICE", "INFO", "DEBUG")' \
		'log_file_name:string' \
		'log_max_size:uinteger' \
		'userid:string' \
		'passwd:string' \
		'port:uinteger' \
		'provisioning_code:string:""' \
		'amd_version:range(1, 6)' \
		'instance_mode:or("InstanceNumber","InstanceAlias")' \
		'session_timeout:uinteger' \
		'notification:bool' \
		'exec_download:bool' \
		'periodic_notify_enable:bool' \
		'enable:bool' \
		'periodic_notify_interval:uinteger'
}

validate_defaults() {
	config_load cwmp

	validate_acs_section || {
		log "Validation of acs section failed"
		return 1;
	}

	[ -z "${url}" -a -z "${dhcp_url}" ] && {
		log "ACS url is empty can't start"
		return 1;
	}

	validate_cpe_section || {
		log "Validation of cpe section failed"
		return 1;
	}

	[ -z "${default_wan_interface}" ] && {
		log "Wan interface is empty"
		return 1;
	}

	return 0;
}

start_service() {
	local enable_cwmp=`uci -q get cwmp.cpe.enable`
	local wan_interface=$(get_default_wan_interface)
	local dhcp_discovery=`uci -q get cwmp.acs.dhcp_discovery`

	if [ "$enable_cwmp" = "0" -o "$enable_cwmp" = "false" ]; then
		log "CWMP is not enabled"
		return 0
	fi

	# Set wan interface if not configured
	set_wan_interface "${wan_interface}"

	# Set dhcp option 43 if dhcp discovery enabled
	if [ "${dhcp_discovery}" == "enable" -o "${dhcp_discovery}" == "1" ]; then
		enable_dhcp_option43 "${wan_interface}"
	fi

	[ -f /sbin/netifd ] && log "Waiting for Network to be started ..." && ubus -t 5 wait_for network.interface
	[ -f /usr/sbin/dnsmasq ] && log "Waiting for DNS Proxy to be started ..." && ubus -t 5 wait_for dnsmasq
	[ -f /etc/config/dhcp ] && log "Waiting for DNS Server(s) ..." && wait_for_resolvfile 20

	# Copy backup data so that if it restart latter on it gets the info
	copy_cwmp_etc_files_to_varstate

	validate_defaults || {
		log "Validation of defaults failed"
		return 1;
	}

	procd_open_instance icwmp
	procd_set_param command "$PROG"
	procd_append_param command -b
	procd_set_param respawn \
		${respawn_threshold:-5} \
		${respawn_timeout:-10} ${respawn_retry:-3}

	procd_set_param watch network.interface
	procd_close_instance
}

stop_service()
{
	copy_cwmp_varstate_files_to_etc
}

reload_service() {
	log "Restarting CWMP client"
	stop
	start
}

service_triggers() {
       procd_add_reload_trigger "cwmp"

       procd_open_trigger
               json_add_array
                       json_add_string "" "interface.update"
                       json_add_array
                               json_add_array
                                       json_add_string "" "run_script"
                                       json_add_string "" "/etc/icwmpd/update.sh"
                               json_close_array
                       json_close_array
                       json_add_int "" "2000"
               json_close_array
       procd_close_trigger
}

