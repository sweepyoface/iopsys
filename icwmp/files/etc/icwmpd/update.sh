#!/bin/sh

log() {
	echo $@ |logger -t cwmp.update -p info
}

handle_icwmp_update() {
	local defwan=$(uci -q get cwmp.cpe.default_wan_interface)
	local vendorspecinf=`ubus call network.interface.${defwan} status | jsonfilter -e "@.data.vendorspecinf"`

	log "Handling dhcp option value ${vendorspecinf}"
	[ -n "$vendorspecinf" ] && {
		local url=""
		local old_url=$(uci -q get cwmp.acs.dhcp_url)

		case $vendorspecinf in
			http://*|https://*)
					url="$vendorspecinf"
				;;
			*)
				for optval in $vendorspecinf; do
				        case $optval in
				                1=*)
				                        url="$(echo $optval | cut -d"=" -f2-)"
				                ;;
				        esac
				done
			;;
		esac

		if [ -n "$url" ]; then
			log "## icwmp url[${old_url}] changed to [${url}]"
			if [ -z "${old_url}" -o "${url}" != "${old_url}" ]; then
				log "Restarting icwmp url[${old_url}] changed to [${url}]"
				uci -q set cwmp.acs.dhcp_url="$url"
				ubus call uci commit '{"config":"cwmp"}'
			fi
		fi
	}
}

handle_icwmp_update
