#!/bin/sh
. /lib/functions.sh
. /usr/share/libubox/jshn.sh

configure_snpwd() {
        local serial_no password
        serial_no="$1"
        password="$2"

        # serial number comprises of 2 parts, vendor id and vendor specific, the vendor id is
        # a string while the vendor specific is a hex, so split the 2 and set accordingly
        local vendor_id vendor_specific
        vendor_id=${serial_no:0:4}
        vendor_specific=${serial_no: -8}

        # attempt to conver vendor_id from string to hex
	vendor_id=$(echo $vendor_id | hexdump -e '4/1 "%02X" "\n"')
	vendor_id=${vendor_id:0:8}

        bdmf_shell -c `cat /var/bdmf_sh_id` -cmd /b/configure gpon onu_sn={vendor_id=$vendor_id,vendor_specific=$vendor_specific}
        if [ -n "$password" ]; then
		password=$(echo $password | hexdump -n ${#password} -e '16/1 "%02X""\n"')
                bdmf_shell -c `cat /var/bdmf_sh_id` -cmd /b/configure gpon password=$password
        fi
}

configure_gpon() {
        local enabled serial_no password
        config_load pon
        config_get enabled globals "enabled"

	if [ "$enabled" == "0" ]; then
		exit
        fi

        config_get serial_no globals "serial_number"
        config_get password globals "password"

        configure_snpwd $serial_no $password
}

start_gpon() {
        if [ -n "$(which gponctl)" ]; then
                configure_gpon
                gponctl start
        fi
}

stop_gpon() {
        if [ -n "$(which gponctl)" ]; then
                gponctl stop
        fi
}

get_gpon_status() {
        json_init
        status="$(gponctl getstate)"
        admin_status="$(echo $status | head -n1 | awk '{print $8;}')"
        json_add_string "admin_status" "$admin_status"
        op_status="$(echo $status | head -n1 | awk '{print $12;}')"
        case $op_status in
        NUMBER)
                op_status="$(echo $status | head -n1 | awk '{print $13;}')"
                ;;
        esac

        op_status=${op_status:1:2}
        json_add_string "operational_status" "$op_status"
        json_dump
}
