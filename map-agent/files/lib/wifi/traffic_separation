#!/bin/sh

. /lib/functions.sh

### Traffic Separation ###

ts_sub() {

	ts_usage() {
		cat <<EOF
Usage: $0 [create|delete|populate]
Traffic Separation related functions.
create fh <iface> <vid>			- create vlan device and rules for wifi fronthaul
create bh <iface> <vid> <profile>	- create vlan device and rules for wifi backhaul
create eth <iface> <vid> <pbits>	- create vlan device and rules for logical ethernet interface
delete <iface>				- delete vlan device
populate eth <iface> <vid>		- add secondary network rules for logical ethernet interface
primary get <iface>			- read primary VID for interface from driver (from Association Response frame IE)
     
EOF
	exit 1
	}

	_get_bridge() {
		local bridge=$(ip link show $1 | grep -o "master [^\s]*" | cut -d ' ' -f 2)
		echo $bridge
	}

	ts_create() {

		_create_vlan_dev() {
			iface=$1				# real iface
			vid=$2					# Vlan ID
			bridge="$(_get_bridge $iface)"		# bridge iface is connected to
			vlan_dev=${iface}_vlan${vid}		# name of vlan device to create

			# remove interface from bridge if it is in one
			[ -n "$bridge" ] && brctl delif $bridge $iface &> /dev/null

			# create vlan device
			vlanctl --mcast --if-create-name $iface $vlan_dev --if $iface --set-if-mode-rg

			# bring the vlan device up and add back to bridge if it was in one
			[ -n "$bridge" ] && brctl addif $bridge $vlan_dev &> /dev/null
			ip link set dev $vlan_dev up

			echo $vlan_dev
		}

		ts_create_fh() {
			iface=$1				# fh iface
			vid=$2					# SSID specific (secondary) or primary vid

			[ -n "$iface" ] && [ -n "$vid" ] || {
				cat <<EOF
Adding FH device requires IFACE and VID.
EOF
				exit 1
			}

			# create vlan device
			vlan_dev=$(_create_vlan_dev $iface $vid)

			# add rules
			# Note: removing these for now since these packets are read directly from the iface anyway
#			vlanctl --if $iface --rx --tags 0 --filter-ethertype 0x888e --set-rxif $vlan_dev --rule-append		# incoming EAPOL
#			vlanctl --if $iface --rx --tags 0 --filter-ethertype 0x886c --set-rxif $vlan_dev --rule-append		# incoming brcm
			vlanctl --if $iface --rx --tags 0 --set-rxif $vlan_dev --push-tag --set-vid $vid 0 --rule-append
			vlanctl --if $iface --tx --tags 1 --filter-txif $vlan_dev --filter-vid $vid 0 --pop-tag --dscp2pbits 0 --rule-append
		}

# Note: Currently not used (will be needed for Profile 1)
		ts_create_bh() {
			iface=$1				# bh iface
			vid=$2					# primary vid
			profile=$3				# agent profile (1 or 2)

			[ -n "$iface" ] && [ -n "$vid" ] && [ -n "$profile" ] || {
				cat <<EOF
Adding BH device requires IFACE, VID and PROFILE (1 or 2).
EOF
				exit 1
			}

			# create vlan device
			vlan_dev=$(_create_vlan_dev $iface $vid)

			# add rules
			case "$profile" in
				1 | profile_1 | profile1 | Profile_1 | Profile1)
					vlanctl --if $iface --rx --tags 0 --set-rxif $vlan_dev --push-tag --set-vid $vid 0 --rule-append
					vlanctl --if $iface --tx --tags 1 --filter-txif $vlan_dev --pop-tag --rule-append
					;;
				2 | profile_2 | profile2 | Profile_2 | Profile2)
					vlanctl --if $iface --rx --tags 1 --set-rxif $vlan_dev --rule-append
					vlanctl --if $iface --tx --tags 1 --filter-txif $vlan_dev --rule-append								# note: already default
					vlanctl --if $iface --tx --tags 0 --filter-ethertype 0x893a --filter-txif $vlan_dev --push-tag --set-vid $vid 0 --rule-append	# note: outgoing IEEE1905.1
					vlanctl --if $iface --tx --tags 0 --filter-ethertype 0x888e --filter-txif $vlan_dev --push-tag --set-vid $vid 0 --rule-append	# note: outgoing EAPOL
					;;
				*)
					ts_delete $iface
					ts_usage
					exit 1
					;;
			esac
		}

		ts_create_bh_drv() {
			iface=$1				# bh iface
			vid=$2					# primary vid
			profile=$3				# agent profile (1 or 2)

			[ -n "$iface" ] && [ -n "$vid" ] && [ -n "$profile" ] || {
				cat <<EOF
Adding BH device requires IFACE, VID and PROFILE (1 or 2).
EOF
				exit 1
			}

			wl vlan_mode 0 &> /dev/null				# make sure this is OFF, else driver won't handle vlan
			wl -i $iface down &> /dev/null
			wl -i $iface map_profile $profile &> /dev/null		# only has effect on profile 2
			wl -i $iface map_8021q_settings $vid &> /dev/null	# can only be set on bBSS (read-only on bSTA)
			wl -i $iface up &> /dev/null
		}

		ts_create_eth() {

			_ts_setup() {
				vlan_bridge=$1
				lan_bridge=$2
				vlan_dev=$3
				lan_dev=${vlan_dev}_lan

				ip link show $vlan_dev &> /dev/null || ip link add $vlan_dev type veth peer name $lan_dev
				brctl addif $vlan_bridge $vlan_dev &> /dev/null
				brctl addif $lan_bridge $lan_dev &> /dev/null
				ip link set $vlan_dev up
				ip link set $lan_dev up
			}

			iface=$1				# Multi-AP Logical Ethernet Interface
			vid=$2					# primary vid
			pbits=$3				# default pbits to apply
			vlan_bridge=$4				# name of ts subsystem bridge
			lan_bridge=$5				# name of node local bridge

			[ -n "$iface" ] && [ -n "$vid" ] && [ -n "$pbits" ] && [ -n "$vlan_bridge" ] && [ -n "$lan_bridge" ] || {
				cat <<EOF
Adding ETH device requires IFACE, VID, PBITS and name of vlan and lan bridges.
EOF
				exit 1
			}

			# create virtual eth device between bridges (if it doesn't exist))
			_ts_setup $vlan_bridge $lan_bridge $iface

			# create vlan device
			vlan_dev=$(_create_vlan_dev $iface $vid)

			# TODO: how to disable returning traffic back to incoming interface?

			# add rules
			vlanctl --if $iface --rx --tags 0 --set-rxif $vlan_dev --push-tag --set-vid $vid 0 --set-pbits $pbits 0 --rule-append
			vlanctl --if $iface --tx --tags 1 --filter-vid $vid 0 --filter-txif $vlan_dev --pop-tag --rule-append
		}

		ts_create_dhcp() {
			_br_setup() {
				local name=$1
				local sinkname=$2
				local vid=$3
				local diff=""

				[ -z "$(uci -q get network.${name}_dev)" ] && {
					uci -q set network.${name}_dev="device"
					uci -q set network.${name}_dev.name="br-${name}"
					uci -q set network.${name}_dev.type="bridge"
					uci -q set network.${name}_dev.bridge_empty="1"
					uci -q add_list network.${name}_dev.ports="${sinkname}"

					diff="1"
				}

				[ -z "$(uci -q get network.${name})" ] && {
					uci -q set network.${name}="interface"
					uci -q set network.${name}.device="br-${name}"
					uci -q set network.${name}.is_lan="1"
					uci -q set network.${name}.proto="static"
					uci -q set network.${name}.ipaddr="192.168.${vid}.1"
					uci -q set network.${name}.netmask="255.255.255.0"

					diff="1"
				}

				[ "$diff" = "1" ] && {
					uci -q commit network
					ubus call network reload
					ubus -t 5 wait_for network.device
				}
			}

			_dhcp_setup() {
				local name=$1

				[ -n "$(uci -q get dhcp.${name})" ] && return

				uci -q set dhcp.${name}=dhcp
				uci -q set dhcp.${name}.interface="${name}"
				uci -q set dhcp.${name}.start="100"
				uci -q set dhcp.${name}.limit="150"
				uci -q set dhcp.${name}.leasetime="1h"
				uci -q set dhcp.${name}.dhcpv4="server"
				uci -q set dhcp.${name}.dhcpv6="server"
				uci -q set dhcp.${name}.ra="server"
				uci -q set dhcp.${name}.ra_slaac="1"
				uci -q add_list dhcp.${name}.ra_flags="managed-config"
				uci -q add_list dhcp.${name}.ra_flags="other-config"

				uci -q commit dhcp
				ubus call uci commit '{"config":"dhcp"}'
			}

			_wan_setup() {
				local name=$1

				config_load firewall

				_process_zone() {
					local section=$1
					local sink=$2
					local name
					local exists=false

					config_get name $section name

					[ "$name" == "lan" ] || return

					_process_list() {
						local value=$1

						[ "$value" == "$sink" ] && exists=true && return 1
					}

					config_list_foreach "$section" network _process_list 

					[ $exists = false ] && {
						uci -q add_list firewall.${section}.network="${sink}"
						uci -q commit firewall
						ubus call uci commit '{"config":"firewall"}'
					}

					return 1
				}

				config_foreach _process_zone zone $name
			}

			vid=$1					# primary vid

			[ -n "$vid" ] || {
				cat <<EOF
VID required to configure DHCP.
EOF
				exit 1
			}

			br_dev=sink_vlan${vid}			# name of vlan device to create


			ip l a sink1_vlan${vid} type veth peer name sink2_vlan${vid}
			vlanctl --mcast --if-create-name sink2_vlan${vid} sink3_vlan${vid} --if sink2_vlan${vid} --set-if-mode-rg
			vlanctl --if sink2_vlan${vid} --rx --tags 1 --filter-vid ${vid} 0 --set-rxif sink3_vlan${vid} --pop-tag --rule-append
			vlanctl --if sink2_vlan${vid} --tx --tags 0 --filter-txif sink3_vlan${vid} --push-tag --set-vid ${vid} 0 --rule-append
			ip l s sink1_vlan${vid} up
			ip l s sink2_vlan${vid} up
			ip l s sink3_vlan${vid} up

			_br_setup $br_dev sink3_vlan${vid} $vid
			_dhcp_setup $br_dev
			_wan_setup $br_dev
		}

	local type=$1
	shift

	case "$type" in
		fh) ts_create_fh $@;;
#		bh) ts_create_bh $@;;
		bh) ts_create_bh_drv $@;;
		eth) ts_create_eth $@;;
		dhcp) ts_create_dhcp $@;;
		--help|help) ts_usage;;
		*) ts_usage; exit 1;;
	esac
	}

	ts_delete() {
		iface=$1

		path=$(ls -d /sys/class/net/${iface}/upper_*)
		[ -z "$path" ] && exit 0
		vlan_dev=${path##*upper_}
		bridge="$(_get_bridge $vlan_dev)"

		vlanctl --if-delete $vlan_dev		# note: also removes rules and removes from bridge

		brctl addif $bridge $iface &> /dev/null
	}

	ts_populate() {
		local type=$1
		shift

		[ "$type" == "eth" ] || {
			cat <<EOF
Populating ETH rules requires IFACE and VID.
EOF
			exit 1
		}

		iface=$1				# Multi-AP Logical Ethernet Interface
		vid=$2					# secondary vid

		path=$(ls -d /sys/class/net/${iface}/upper_*)
		[ -z "$path" ] && exit 1
		vlan_dev=${path##*upper_}

		# add rules
		vlanctl --if $iface --rx --tags 1 --filter-vid $vid 0 --set-rxif $vlan_dev --rule-append		# note: retain secondary vids
		vlanctl --if $iface --tx --tags 1 --filter-vid $vid 0 --filter-txif $vlan_dev --rule-append		# note: already default
	}

	ts_primary() {
		local func=$1
		shift

		case "$func" in
			get) vid=$(wl -i $1 map_8021q_settings); echo ${vid##* };;
			*) ts_usage; exit 1;;
		esac
	}

	ts_unicast() {
		local mac=$1
		local iface=$2

		# TODO: improve?
		res=$(ebtables -t broute -L | grep -i "$mac" | grep "$iface")
		[ -n "$res" ] && return

		ebtables -t broute -D BROUTING -d "$mac" -p 0x893a -j DROP
		ebtables -t broute -I BROUTING -i "$iface" -d "$mac" -p 0x893a -j DROP
	}

	ts_multicast() {
		local iface=$1

		# TODO: improve?
		res=$(ebtables -t broute -L | grep -i "1:80:C2:00:00:13" | grep "$iface")
		[ -n "$res" ] && return

		ebtables -t broute -D BROUTING -d 01:80:C2:00:00:13 -p 0x893a -j DROP
		ebtables -t broute -D BROUTING -i "$iface" -d 01:80:C2:00:00:13 -p 0x893a -j DROP
		ebtables -t broute -I BROUTING -i "$iface" -d 01:80:C2:00:00:13 -p 0x893a -j DROP
	}

	local func=$1
	shift

	case "$func" in
		create) ts_create $@;;
		delete) ts_delete $@;;
		populate) ts_populate $@;;
		primary) ts_primary $@;;
		unicast) ts_unicast $@;;
		multicast) ts_multicast $@;;
		--help|help) ts_usage;;
		*) ts_usage; exit 1;;
	esac
}
